import argparse
import requests
import re
import textwrap

html_tags = re.compile('<.*?>')
# https://rollenspiel.social/@mforester/113997129488085463
fedi_regex = re.compile('(https?://.*)/(.*)/(.*)')


class FediReply:

    UserName: str
    Message: str
    Id: str

    def __init__(self, user_name, message, id) -> None:
        self.UserName = user_name
        self.Message = message
        self.Id = id


def clean_html(html):
    return re.sub(html_tags, '', html)


def get_replies(server: str, message_id: str):
    res = requests.get(f"{server}/api/v1/statuses/{message_id}/context")
    return res.json()


def parse_reply(reply) -> FediReply:
    return FediReply(reply['account']['display_name'], reply['content'], reply['id'])


def build_reply_tree(replies) -> dict[str,list[FediReply]]:
    tree = {}
    if 'descendants' not in replies:
        return tree

    for reply in replies['descendants']:
        reply_to_id = reply['in_reply_to_id']
        if not reply_to_id in tree:
            tree[reply_to_id] = []
        fedi_reply = parse_reply(reply)
        tree[reply_to_id].append(fedi_reply)
    return tree


def console_print(reply: FediReply, indent):
    user = reply.UserName
    message = clean_html(reply.Message)
    base_indent = ' '*int(indent)*4
    msg_indent = base_indent + f'{user}: '
    wrapper = textwrap.TextWrapper(
        initial_indent=msg_indent,
        subsequent_indent=' '*len(msg_indent),
        width=70
    )
    print(wrapper.fill(message))


def print_replies(tree:dict[str,list[FediReply]], parent_message_id, output_func=console_print, indent=0):
    if parent_message_id not in tree:
        return
    for reply in tree[parent_message_id]:
        output_func(reply, indent)
        print_replies(tree, reply.Id, output_func, indent+1)


def parse_server_and_message_id(url: str) -> tuple[str,str]:
    match = re.match(fedi_regex, url)
    if not match:
        raise ValueError(f'Could not parse server and message ID from url: {url}')

    return match.group(1), match.group(3)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('url', help='A URL to a Mastodon post.')
    args = parser.parse_args()
    server, message_id = parse_server_and_message_id(args.url)
    replies = get_replies(server, message_id)
    tree = build_reply_tree(replies)
    print_replies(tree, message_id)


if __name__ == '__main__':
    main()
