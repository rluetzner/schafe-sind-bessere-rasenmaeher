#!/bin/bash
set -o errexit
set -o nounset
set -o pipefail

SCRIPT_PATH=$(dirname $(realpath "$0"))

pushd $SCRIPT_PATH

trap popd EXIT

git fetch origin
git reset --hard origin/main
git submodule update

docker pull hugomods/hugo:latest
docker run --rm -v "$(pwd):/src" hugomods/hugo:latest build --minify
