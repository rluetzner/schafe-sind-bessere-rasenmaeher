---
title: "Hack all the things - PS Vita"
date: 2025-01-23T16:20:46+01:00
draft: false
categories: ["tech"]
tags: ["hacking","playstation","vita"]
license: cc-by-nc-sa
mastodonlink: https://rollenspiel.social/@mforester/113878839943227631
---

Retrogaming and devices for this specific hobby have been part of my Fediverse bubble for quite some time now. And like most people, I eventually get jealous and want one of my own.

But that is not the story why I hacked my PS Vita. That is actually the short story in which I go looking for my Vita again, which is sitting in a drawer ready to rock some classic NES and GameBoy games.

I did the actual hacking much earlier, when Sony announced that they were shutting down the PlayStation Store on the PS Vita back in 2021. I immediately thought that this meant all my digital games would be lost forever. And while I hadn't touched the Vita in years, I still wasn't going to let all that hard earned money go to waste.[^psn-store]

[^psn-store]: Apparently they merely _delisted_ all PS Vita, PS3 and PSP titles, meaning that you cannot buy new titles, but should still be able to download anything you already bought.

The only problem with that: the Memory Stick Pro Duo, Sony's proprietary memory card used by the PS Vita.

I just checked and apparently there's a 64 GB card in mine, which is plenty of space -- but then I also had plenty of games. And we all know how quickly 64 GB can be filled, especially with games.

I think the Vita supported up to 128 GB, which would probably have been enough. However, there's another thing with the MS Pro Duos: they were fucking expensive!!!

I can't remember details, but I do remember that the cards were significantly more expensive than an SD or Micro SD.

Apparently there are now MS Pro Duo to Micro SD adapters, but I can't remember those being around 4 years ago, when I was scrambling for a solution that ideally wouldn't drain too much of my money.

What I found instead was PS2Vita!

![My SD2Vita Pro adapter next to the 128 GB Micro SD card that goes into it.](ps2vita.webp)

With this adapter I could keep my 64 GB MS Pro Duo in place and add another big chunk of storage to it, by plugging this adapter into the game card slot of the PS Vita. Genius!

There was however one catch -- and you might see where I'm going with this.

I had to hack my PS Vita.

I'm not going to give step by step instructions, because there's a way better site for that: https://vita.hacks.guide .

I don't remember the specifics, but I do remember that actually hacking the device was incredibly easy. The steps are well documented and easy to follow.

I kinda understand what's going on, including that I'm exploiting a software vulnerability and running unknown code on my device. It might not be for everybody, but I chose to trust the people involved and I'm amazed and happy with the result.

The one thing I hadn't quite thought of was, that the new adapter occupies the game slot, so I can no longer play my original cartridges. However, I quickly discovered that I can just dump these with my hacked Vita and start them from the MS Pro Duo.

![My PS Vita booting into custom firmware.](ps-vita-booting.webp)

However, after all that work I think I played with it for a few days and then put it away to be forgotten for quite a while.

And that's where we come back to the retrogaming handhelds, where it seems like every day a new one is announced.

I was super jealous when I saw what these could do and I was almost ready to buy one. In fact, I was looking at a device that looked a bit like the PS Vita, which made me remember...

**I already have a retrogaming handheld!**

And here's where I gush a bit about how much fun it is to play with it. I think the Vita feels really good in the hands and it runs anything up to original PlayStation games just fine (i.e. anything from the 16-bit era and before). I've had mixed results with emulating PlayStation games, where some ran fine, but most ran too slow for me to enjoy them.

But I'm happy enough with having a bunch of old Nintendo and Sega classics to play on the go. With the Nintendo Switch I often debate if I can spare the space in my backpack, but the Vita is so much smaller and flatter, so it's a no-brainer.

And you know what you can do with the Vita? Connect your Bluetooth headphones! (Side-eyes to the Switch, where they eventually patched this feature in after a few years.)

And once you're done with all the classics, there's still tons of homebrew software being developed. A few games, but many, many amazing tools like emulators, an ftp server, theme managers, alternative launchers, the obligatory Doom port, DevilutionX (a port of Diablo), ...

> If you're interested in hacking your retro handhelds, I can recommend following the news on https://wololo.net . I've discovered a bunch of neat homebrew software from this site.

Just look at this beautiful thing!

![The Retro Flow Launcher running on the PS Vita.](ps-vita-retro-flow.webp)
