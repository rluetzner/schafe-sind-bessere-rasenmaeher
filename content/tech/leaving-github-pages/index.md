---
title: "Leaving GitHub Pages"
date: 2025-02-12T09:20:46+01:00
draft: false
categories: ["tech"]
tags: ["github","migration","self-hosting","traefik","nginx","hugo"]
license: cc-by-nc-sa
mastodonlink: https://rollenspiel.social/@mforester/113991016153312110
---

I finally managed to move my blog off of GitHub for good!

I mostly followed the call to leave GitHub, shortly after they announced Copilot, the LLM they illegally trained on all the code on GitHub in secret. Almost all my code is now hosted on [Codeberg](https://codeberg.org/) with backup copies on a [Forgejo](https://forgejo.org/) instance in my homelab.

But I took my sweet time moving the blog, because it was just so very convenient to have it on GitHub.

1. It was hosted on GitHub Pages, which means I didn't have to worry about
    - DNS,
    - storage space,
    - requesting and refreshing certificates or
    - handling insane A.I. crawlers.
2. With every commit I pushed, a GitHub workflow[^the-problem-with-github-workflows] would ensure that my site was rebuilt and pushed to GitHub Pages.

[^the-problem-with-github-workflows]: I hate GitHub workflows! I'm convinced that they are intentionally a convoluted mess of `plugin@version` with a bunch of variables, so it's harder to figure out what's actually going on. It might be different when you've written the pipeline yourself, but I merely copied the workflow I had from somewhere else without putting in the effort to understanding what happens under the hood.  
Or in other words: before starting this migration, I had never ever run the `hugo build` command.

I kinda like automating things and figuring out build pipelines, so point 2 wasn't much of an issue. But it took me a while to decide where to go from GitHub Pages.

The obvious choice would be [Codeberg Pages](https://docs.codeberg.org/codeberg-pages/), but I wasn't entirely happy with it. The biggest drawback is that Codeberg only allows users to host a single website in a `pages` Git repository, whereas GitHub supports multiple websites.

Not the biggest issue, seeing as I only have the one blog. But if I ever do start a new blog in the future, I'd like to keep the process as similar as possible, which I can't with Codeberg.

The other thing that bothered me was, that I'd be drawing all the A.I. crawlers gobbling up my site to Codeberg. Again, my blog is not a high traffic site, but it somehow didn't feel right.

So these thoughts tumbled around in my brain for a while, went away and then bubbled up again last week. What was different this time, is that I'd already fixed the reverse proxy for my homelab to block any known A.I. crawler user agents.

I did that a while ago with the [ai.robots.txt Git repository](https://github.com/ai-robots-txt/ai.robots.txt). Once a day I run a Python script to grab the `robots.txt` file from the project and generate a block of Nginx code to keep these vultures out.

```Python
import requests
import os

def main():
    url = 'https://raw.githubusercontent.com/ai-robots-txt/ai.robots.txt/main/robots.txt'
    res = requests.get(url)
    if res.status_code != 200:
        print(f'Something went wrong: {res.status_code} - {res.content}')
        os.exit(1)
    body = res.text
    lines = list(map(lambda l: l.replace('User-agent: ', '') , filter(lambda l: l and 'User-agent' in l, body.split('\n'))))

    aiUserAgents = '|'.join(lines)
    with open('ai-crawlers.conf', 'w') as f:
        f.write(f'''if ($http_user_agent ~* "({aiUserAgents})") {{
    return 403;
}}
''')
    print('Generated ai-crawlers.conf.')

if __name__ == '__main__':
    main()
```

> I just saw that they have a `robots.json` file too, which would probably make things even easier.

I use an Ansible playbook to push this file to my VPS, where I include it in my Nginx configs like this:

```nginx
server {
        listen [::]:443 ssl;
        listen 443 ssl;

        ssl_certificate /etc/ssl/cert.cert;
        ssl_certificate_key /etc/ssl/cert.key;

        server_name example.com;

        location / {
                include ai-crawlers.conf;

                # Rest of the config goes here.
                [...]
        }
}
```

With this in place, it was much easier from me to decide to host the blog myself.

The next thing to tackle was that everything in my homelab is running in Docker containers, including [Traefik](https://traefik.io/). Traefik works well as a proxy, but it's **not** a webserver. So I needed something else to serve my static files.

That was pretty easy to solve, as I could just `hugo build` the website and bind it to a path in an Nginx container.

```bash
hugo build
docker run -it -v "$(pwd)/public:/usr/share/nginx/html" -p 8080:80 nginx:latest
```

This worked great and I was able to browse my website, including all images and the RSS feeds from a server in my homelab. I edited the `docker-compose.yml` to include all the necessary labels to hook up routes in Traefik, setup a cronjob to pull changes from Git and run `hugo build` and moved on.

With the fun part out of the way, I looked into how to request a Let's Encrypt certificate for another domain with Traefik. Luckily that was straightforward too, as I had already solved it in the past. I just had to add two new labels to my `docker-compose.yml`.

```yaml
labels:
[...]
- traefik.http.routers.traefik-secure.tls.domains[1].main=schafe-sind-bessere-rasenmaeher.de
- traefik.http.routers.traefik-secure.tls.domains[1].sans=*.schafe-sind-bessere-rasenmaeher.de
```

Moving further upwards in my stack, I then had to add another server config to Nginx running on my VPS.

At this point things might get a bit confusing, so I'll try to sketch out how this all ties together.[^yes-i-know]

[^yes-i-know]: Yes, I know this seems overly complicated, but I've got a good reason for doing it this way: I run Pi-Hole at home as a DNS server and I have assigned subdomains to every single service, not all of which are publicly available.

![A flowchart explaining that Nginx runs on a VPS, which accesses Traefik running as a Docker container on my Pi 4, which proxies Nginx running as a Docker container on the same Pi, which serves my blog from a Docker bind volume.](blog-stack.svg)

With this I had everything in place to start the migration.

1. Update DNS entries for `schafe-sind-bessere-rasenmaeher.de`.
2. Restart the Traefik Docker container to request a new certificate from Let's Encrypt for `schafe-sind-bessere-rasenmaeher.de`.
3. Restart the Nginx Docker container, so Traefik routes requests made to `schafe-sind-bessere-rasenmaeher.de` to it.

At this point everything was wired up correctly and I had a certificate for `schafe-sind-bessere-rasenmaeher.de` on my Pi. However, I still had to transfer the certificate and key to the VPS. Again, this was a problem that I had already solved in the past and it's part of the Ansible playbook I mentioned earlier.

I'm not going to go through the entire playbook, but the steps to get certificates out of Traefik are interesting enough to explain them. Contrary to Nginx or Apache, the certificates and keys are not in separate files, but together in a JSON file. It's JSON so `jq` _to the rescue!!_ Once we've got the value, we can decode it with `base64`.

```bash
jq -r '.cloudflare.Certificates[0].certificate' ./traefik-ssl-certs/cloudflare.json | \
    base64 -d >
    cert.cert
jq -r '.cloudflare.Certificates[0].key' ./traefik-ssl-certs/cloudflare.json |
    base64 -d >
    cert.key
```

And with that, I was done. It may look like a lot of steps, but ultimately it didn't take me much more than 30 minutes. And it would have been much faster, if I hadn't have done it typing on my phone.
