---
title: "Static comments for my static blog"
date: 2025-02-25T10:20:46+01:00
draft: false
categories: ["tech"]
tags: ["hugo","mastodon","fediverse","comments"]
license: cc-by-nc-sa
mastodonlink: https://rollenspiel.social/@mforester/114064749333637041
---

I recently came upon a few blog posts that talked about adding comments to a static site.

- Here's [Maho Pacheco](https://hachyderm.io/@mapache) hooking up their blog to the Fediverse by [implementing the necessary ActivityPub endpoints](https://maho.dev/2024/02/a-guide-to-implement-activitypub-in-a-static-site-or-any-website/).  
  It's a very thorough and exciting series of posts, but it's much, much more than I intended for my blog.
- Then there's [Beej](https://mastodon.sdf.org/@beejjorgensen) describing how to [dynamically load replies from Mastodon with JavaScript](https://beej.us/blog/data/mastodon-comments/).  
  This is much closer to what I had in mind. In fact I just recently added metadata to my posts that allows me to link to a Mastodon post. The only thing I wasn't too happy about was the JavaScript.  
  Ultimately I don't think that adding JavaScript to this blog to enable comments would have been a bad choice. I actually think that there are some benefits to it, which I'll get into in a bit. But I'm also somewhat proud of myself for getting by without any JavaScript for so long and wanted to keep it that way.

{{<aside>}}
I want to mention a few things before you waste your time, dear reader.

Replies on Mastodon and generally comments on a blog post happen dynamically. By choosing **not** to use JavaScript, I have no other way of adding comments than rebuilding the entire site. If you're looking for a dynamical solution, you'll want to look at [Beej's post](https://beej.us/blog/data/mastodon-comments/).

I have a setup where my site is rebuilt every 15 minutes and having comments update in that interval is fine for me.

I assume that most people do **not** have this setup and rather rebuild their static sites when certain events happen (e.g. a Git commit). This solution is probably not for you, but you might still learn something worthwhile.
{{</aside>}}

The most important part I found in Beej's post was the following Mastodon[^mastodon] API endpoint.

[^mastodon]: Here's the thing: I post from a Mastodon instance, so the API I'm using is Mastodon specific, **but** I assume that the same process or at least something very similar will work just as well for other Fediverse services.

```bash
https://<instance-url>/api/v1/statuses/<post-id>/context
```

With a `GET` request to this URL we can get a JSON reply containing all replies to the post with the given ID.

I started out with

```bash
curl https://rollenspiel.social/api/v1/statuses/113997129488085463/context | jq . > replies.json
```

and had a look at the properties. I quickly abandoned my Bash script for Python and ultimately ended up with [get-replies.py](https://codeberg.org/rluetzner/schafe-sind-bessere-rasenmaeher/src/branch/main/get-replies.py). When called with a link to a Mastodon post

```bash
python3 get-replies.py https://rollenspiel.social/@mforester/113997129488085463
```

the script will print all the replies to the given post on the console. Replies are sorted into threads and are indented to visualize individual conversations.

This experiment helped me get familiar with the JSON data and uncovered a thing that I hadn't considered before: if I want to group replies into threads, I'll need recursion.

The relevant bit in the Python code looks something like this.

```python
def print_replies(reply_dict, parent_message_id, indent=0):
    if parent_message_id not in reply_dict:
        return
    for reply in reply_dict[parent_message_id]:
        print_reply(reply, indent)
        print_replies(reply_dict, reply.Id, indent+1)
```

The code relies on `reply_dict`, which is a dictionary that groups replies by their `in_reply_to_id`.

1. If there are no replies to `parent_message_id` the function returns immediately (i.e. _end recursion_).
2. If there **are** replies, iterate over all replies that were made to `parent_message_id` and for each
    - print the reply and
    - call the function again, but with the reply's ID as `parent_message_id` (and increase the `indent`ation).

Luckily [Hugo](https://gohugo.io) does allow for [recursion in templates](https://discourse.gohugo.io/t/recursion-in-hugo/36337). Here's an excerpt from what I ended up with.

```go
{{ template "recursive-comments" (dict "comments" $data.descendants "replyTo" $messageId "indent" 0) }}

{{ define "recursive-comments" }}
  {{ $comments := .comments }}
  {{ $indent := .indent }}

  {{ range where .comments ".in_reply_to_id" .replyTo }}

    <!-- a lot of divs, css classes and other boring stuff -->

    {{ template "recursive-comments" (dict "comments" $comments "replyTo" .id "indent" (add $indent 1)) }}
  {{ end }}
{{ end }}
```

Let me break this down a bit from top to bottom and explain some of the pitfalls.

I need multiple values in my `template`, which I can initialize as a [dictionary](https://gohugo.io/functions/collections/dictionary/). The syntax is

```go
dict KEY VALUE ...
```

, so we end up with a dictionary that has all the replies, a reply ID and an indentation.

The `define`d template is pretty similar to the Python code I showed above. The most obvious difference is

```go
{{ $comments := .comments }}
{{ $indent := .indent }}
```

. This might look weird at first. I already have these values in the dictionary, so why am I assigning them to local variables?

Hugo uses `.` as a reference to the current _scope_ object[^scope]. So once we enter the `range` loop our `.` will no longer refer to the dictionary, but to the current iteration object (a reply). And a reply doesn't have a `.comments` or an `.indent` property. I fell into this trap and it took me quite a while to figure out what was going on.

[^scope]: I'm sure there's a better name for that.

I also have no explicit `return` in my recursion, but the recursion will stop once we no longer any replies that are replies to a parent ID.

The only thing that's missing is the actual data, which we can get by using [`resources.GetRemote`](https://gohugo.io/functions/resources/getremote/) and [`transform.Unmarshal`](https://gohugo.io/functions/transform/unmarshal/).

With the hard part out of the way, all we have to do is write a little bit of HTML `div`s and CSS classes and...

Okay, I'm joking. This took me way longer than getting and transforming the comments and there's absolutely nothing I can teach you about HTML and CSS that you couldn't learn better somewhere else.

So with that, I'll just show you the way to the entire [comments template](https://codeberg.org/rluetzner/schafe-sind-bessere-rasenmaeher/src/branch/main/layouts/partials/comments.html) and [additional CSS](https://codeberg.org/rluetzner/schafe-sind-bessere-rasenmaeher/src/branch/main/static/css/additional.css) I wrote.

Now, let's quickly talk about the up- and downsides of this approach.

Beej's approach does require JavaScript, but the entire comments section is optional. People that are interested in the comments are free to hit a button and have the comments load with a request sent from **their** machine. Especially on blogs with low traffic, it makes sense to have the visitor decide and keep requests to a Mastodon instance to a minimum.

Meanwhile, my approach does 1 web request **per post with a Mastodon link** every 15 minutes. I still think this is reasonable as it currently amounts to 25 requests per hour. It scales linearly with the amount of posts. I guess on a high-traffic site my solution might make more sense.[^aware]

[^aware]: Yes, I'm aware that my blog is anything but a high-traffic site.

Beej has also already considered moderation, which they solved with a static list of blocked account names or IDs. While my implementation doesn't do that yet, it is trivial to add. I'll probably add this soon, even though I hope I won't have to use it.

Both approaches have the benefit of "storing" the comment data on Mastodon as replies to a post. We don't need no database! This also means that comments will automatically update when posts on Mastodon are deleted or expire. I think that's pretty neat!
