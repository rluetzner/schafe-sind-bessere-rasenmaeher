---
title: "Hack all the things - Nintendo 3DS"
date: 2025-02-13T10:20:46+01:00
draft: false
categories: ["tech"]
tags: ["hacking","nintendo","3ds"]
license: cc-by-nc-sa
mastodonlink: https://rollenspiel.social/@mforester/113997129488085463
---

I previously talked about [hacking my PS Vita](../hack-all-the-things-ps-vita), which is where my device hacking journey started. My next stop was the Nintendo 3DS that lay forgotten in my cellar and collected dust.

I bought a Nintendo DSi shortly after it came out and replaced it with the New 3DS later. The biggest additions to the 3DS over a DSi were the stereoscopic 3D screen on the top -- which is still super impressive to this day. The **New** 3DS came with a builtin second control _stick_ and two additional shoulder triggers, all of which was previously sold as an accessory called the [Circle Pad Pro](https://en.wikipedia.org/wiki/Nintendo_3DS#Circle_Pad_Pro). Well, the stick is more of a _nub_ and it feels really weird for anything that requires dual stick controls.

{{<aside>}}
The DS models are confusing as hell:

- Nintendo DS
- Nintendo DS Lite
- Nintendo DSi
- Nintendo 3DS
- Nintendo 3DS XL
- New Nintendo 3DS
- New Nintendo 3DS XL

I'm not even sure if that's all the revisions.
{{</aside>}}

While I played a bunch of things on the DSi, the 3DS never saw that much use. Mirroring how I ended up with a GameCube for _Resident Evil Remake_, I bought the New 3DS to play _Resident Evil - Revelations_ (required the second stick), because I'm a huge _Resident Evil_ nerd.[^resident-evil-revelations]

[^resident-evil-revelations]: Incidentally _Resident Evil - Revelations_ is a game that I own on multiple platforms and to this day haven't finished once.

_Revelations_ wasn't all that great, **but** the 3DS made me finally appreciate _The Legend of Zelda - Ocarina of Time_. I made multiple attempts at playing the N64 original, but it was the 3D remaster that got its hooks into me and made me finish the game.

Interestingly enough _Ocarina of Time_ wasn't the only remaster that suddenly caught my attention. While the original _Starwing_ (_Star Fox_) and _Lylat Wars_ (_Star Fox 64_)[^star-fox-64] left me cold, _Star Fox 64 3D_ was amazing and I spent multiple evenings exploring all possible routes. Just thinking about these games makes me want to play them again.

[^star-fox-64]: Apparently there was a German company called StarVox and Nintendo was concerned that the names might be too similar, so they were renamed in some European countries.

Hacking a 3DS is pretty easy and well documented on https://3ds.hacks.guide/ . The guide I linked is specifically for the 3DS models. If you're looking to hack a DSi model instead, you'll have to follow [slightly different steps](https://dsi.cfw.guide/).

Although Nintendo has since shut down the eShop and online services for 3DS, this was not my main motivation to hack the device. In fact...I didn't really have any specific motivation other than that I wanted to tinker with it and try out some homebrew.

There is some really cool homebrew software available, although my joy of discovery was a bit lessened by the fact that I knew almost all the tools and emulators from the PS Vita already. You've got [Devilution X (a Diablo 1 port)](https://github.com/diasurgical/DevilutionX), [RetroArch](https://retroarch.com/index.php), [ftpd](https://github.com/mtheall/ftpd)[^ftpd-vita], etc. which are all amazing and helpful tools, but they just didn't tickle me the same way when I saw them again on the 3DS.

[^ftpd-vita]: After writing that, I realized that ftpd was **not** available on the Vita. However, VitaShell includes an FTP server, so the feature itself wasn't anything special.

There's also [ScummVM](https://docs.scummvm.org/en/latest/other_platforms/nintendo_3ds.html), which I was really looking forward to. Sadly there are some games that the 3DS simply cannot run, amongst them the entire _Blackwell_ series, which I was really looking forward to.[^scumm-vm-phone]

{{<aside>}}
[Games That I Missed](https://rollenspiel.social/@GamesMissed@mastodon.social), a friend from Mastodon, has informed me that this has since been fixed and as of ScummVM 2.9.0 the games are perfectly playable.
{{</aside>}}

[^scumm-vm-phone]: I ultimately ended up playing them on my phone. Not the best experience, but it'll do in cases where you don't have access to a PC with a mouse.

And of course you can backup your cartridges, install ROM hacks / mods and also use custom themes for the home menu.

![An opened 3DS console showing the home menu with an Ori and the Blind Forest theme.](3ds-home-theme.webp)

If I'm being honest, for me the hacked 3DS is ultimately still the ideal way to play Nintendo DS and 3DS games and not much more. I messed around with emulating DS games on my phone and while it works okay -- I replayed the _Professor Layton_ and _Ace Attorney_ games, as well as _Ghost Trick_ that way -- it's best to play them on the original hardware, with the two separate screens and the touch screen and pen.

{{<aside>}}
I only learned about this after I published the post, but I wanted to draw attention to [open_agb_firm](https://github.com/profi200/open_agb_firm). I tried GBA emulation on my 3DS and it worked fine, but games ran a bit slow.

This project unlocks access to the builtin GBA hardware in a 3DS. You'll need to reboot your console to access it, but from there you can run ROMs directly from the SD card, save your game and more.

It's still in beta, but its worth keeping an eye on.
{{</aside>}}

However, what might be a game changer for some is the fact that [Pretendo](https://pretendo.network/) have revived and continue supporting some of the online services for the Nintendo 3DS and Wii U that Nintendo shut down.

I've never been much into online multiplayer, but it's hard to overstate the effort that went into and is still going into this! And it's very simple to setup. I installed the Pretendo homebrew app, changed servers from Nintendo to Pretendo and played a few sessions of online Mario Kart. There were some significant waiting times, but I guess that just not many people are playing Mario Kart on their 3DS anymore.

{{<aside>}}
Yet another correction, but people need to know about this.

[Seasons of Jason](https://layer8.space/@killyourfm), another friend on Mastodon, replied to this post with a link to [CTGP-7, an INSANE Mario Kart 7 modpack](https://ctgp-7.github.io/).

I haven't tried it yet, but apparently this has reinvigorated the Mario Kart 7 playerbase.
{{</aside>}}

Another amazing thing: StreetPass is still working. I also just learned about [NetPass](https://gitlab.com/3ds-netpass/netpass), which lets you rendezvous with other people on online servers.

And while the battery of the 3DS isn't much to write home about (it runs out after maybe 3 or 4 hours depending on the game), I'm astonished to find that it still holds a **full** charge after I left it powered down for months before writing this. The battery usage can be improved by turning off WiFi, although that will prevent StreetPass from working.

While it felt good to _liberate_ the 3DS, I ultimately felt a bit disappointed because it didn't add much to what I could do already with a hacked PS Vita. This won't be the last disappointment in my journey to _hack all the things_ and overall I still feel like it was worth doing.
