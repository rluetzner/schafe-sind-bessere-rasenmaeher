#!/bin/bash
set -o errexit
set -o pipefail
set -o nounset

git diff --exit-code > /dev/null && echo "No changes" && exit 0

# Create a new commit on a new branch.
git checkout -b "${PULL_REQUEST_BRANCH}"
git config --global user.name 'Codeberg CI'
git config --global user.email 'noreply@codeberg.org'
git commit -a -m "Update robots.txt from ai.robots.txt project" -m "See https://github.com/ai-robots-txt/ai.robots.txt"
git remote set-url origin "https://$CB_TOKEN@codeberg.org/$CI_REPO_OWNER/$CI_REPO_NAME.git"
git push --force --set-upstream origin "${PULL_REQUEST_BRANCH}"

# This is actually a Woodpecker CI plugin.
# However, Woodpecker pipelines are currently not able to set plugin values
# from previous steps.
# We need a way to dynamically set the pull request title and body, so we used
# env variables instead and build the project ourselves.
git clone https://codeberg.org/JohnWalkerx/gitea-pull-request-create-plugin
(cd gitea-pull-request-create-plugin; go build)

export PLUGIN_GITEA_ADDRESS='https://codeberg.org'
export PLUGIN_GITEA_TOKEN="${CB_TOKEN}"

export PLUGIN_OWNER="${CI_REPO_OWNER}"
export PLUGIN_REPO="${CI_REPO_NAME}"
export PLUGIN_BRANCH="${PULL_REQUEST_BRANCH}"
export PLUGIN_BASE_BRANCH='main'

export PLUGIN_PR_TITLE="Update robots.txt from ai.robots.txt project"
PLUGIN_PR_BODY=$(cat << EOF
See https://github.com/ai-robots-txt/ai.robots.txt
EOF
)
export PLUGIN_PR_BODY

gitea-pull-request-create-plugin/gitea-pull-request-create-plugin
